import login from '../components/login.vue'
import register from '../components/register.vue'
import Header from '../components/Header.vue'
import mainBody from '../components/mainBody.vue'
import pageNotFound from '../components/pageNotFound.vue'
import details from '../components/details.vue'
import detailsQ from '../components/detailsQ.vue'
const routes = [
    {
        name: 'login',
        path: '/login',
        component: login,
        meta: { auth: false }
    },
    {
        path: '/',
        redirect: 'login'
    },
    {
        name: 'register',
        path: '/register',
        component: register,
        meta: { auth: false }
    },
    {
        path: '*',
        name: 'pageNotFound',
        component: pageNotFound,
        meta: { auth: false }
    },
    {
        name: 'app',
        path: '/app',
        component: Header, // () => import('../components/login.vue') ,
        children: [
            {
                name: 'home',
                path: 'home',
                component: mainBody,    
                meta: { auth: true }
            },
            {
                name: 'detailsssss',
                path: 'detailsssss/:employeeeeeeid',
                component: details,
                meta: { auth: true }
            },
            {
                name: 'detailsQ',
                path: 'detailsQ/',
                component: detailsQ,
                meta: { auth: true }
            }
        ]

    },
]

export default routes 