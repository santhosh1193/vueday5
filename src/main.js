import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import store from './store'

import route from './routes'

import 'bootstrap/dist/css/bootstrap.min.css'
import BootstrapVue from 'bootstrap-vue'



import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const router = new VueRouter({
  routes: route // short for `routes: routes`
})


router.beforeEach((to, from, next) => {
  window.console.log(JSON.parse(window.sessionStorage.getItem('userData')))
  const autherisation = JSON.parse(window.sessionStorage.getItem('userData'));
  const checkRegisterUser = JSON.parse(window.localStorage.getItem('registeredUsers')) || []
  let validateUser = false;
  if (autherisation) {
    validateUser = true
  }
  let checkValidRegistrationUser = false
  checkRegisterUser.forEach((userReg) => {
    if(autherisation.name === userReg.username){
      checkValidRegistrationUser = true
    }
    });
  window.console.log(validateUser , checkValidRegistrationUser)
  window.console.log("mata data : " + to.meta.auth)
  
    if (to.meta.auth && validateUser && !checkValidRegistrationUser) {
      next('/');
    } else {
      next();
    }


})


Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(VueRouter)
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
