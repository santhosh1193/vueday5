import vue from 'vue'
import vuex from 'vuex'
import axios from 'axios'


vue.use(vuex)

const stateTest = new vuex.Store({
    state: {
        posts: null,
        post:null,
        users:["santhosh","nagarju"]
    },
    actions: {
        async getUsers(store) {
            var usersList = await axios.get('https://jsonplaceholder.typicode.com/posts')
            store.commit('getUserMutation', usersList.data)
        },
        async getUserDetails(store, payload) {
            var usersList = await axios.get(`https://jsonplaceholder.typicode.com/posts/${payload}`)
            store.commit('getUserDetailsMutation', usersList.data)
        },
    },
    mutations: {
        getUserMutation(state, payload) {
            state.posts = payload;
        },
        getUserDetailsMutation(state, payload) {
            state.post = payload;
        }
    },
    getters: {
        getUsersGetters(store) {
            return store.posts
        },
        getUserDetailsGetters(store) {
            return store.post
        },
    }


})

export default stateTest